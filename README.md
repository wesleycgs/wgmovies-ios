# Aplicativo de exibição de Catálogo de Filmes
Baseado em consultas nas APIs do trakt, tmdb e imdb.

## Funcionalidades
- Filtrar por filmes mais populars, mais vendidos e mais vistos.
- Ordenar por nome, data de lançamento ou avaliação média.
- Busca

### Desenvolvimento
- Paginação, com limite na quantidade de livros por busca.
- Aplicação universal, para iPhone e iPad.
- Landscape e portrait.
- O tamanho dos cards dos filmes se adaptam à largura do dispositivo, mantendo o aspecto. 
- 3D Touch no filme na lista.
- Tamanho de fonte dinâmica (Acessibilidade).
