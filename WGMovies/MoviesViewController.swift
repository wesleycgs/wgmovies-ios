//
//  MoviesViewController.swift
//  WGMovies
//
//  Created by Wesley Gomes on 28/08/17.
//  Copyright © 2017 Wesley Gomes. All rights reserved.
//

import UIKit
import Alamofire


class MoviesViewController: UIViewController {

    // MARK: - Outlets
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet var searchBarButtonItem: UIBarButtonItem!
    @IBOutlet var listBarButtonItem: UIBarButtonItem!
    @IBOutlet weak var orderBarButtonItem: UIBarButtonItem!
    
    var movies: [Movie] = []
    var moviesSearch: [Movie] = []
    var searchList = SearchList.popular
    var order = Order.title {
        didSet {
            switch order {
            case .title:
                if isSearching {
                    moviesSearch = moviesSearch.sorted(by: { $0.title < $1.title })
                } else {
                    movies = movies.sorted(by: { $0.title < $1.title })
                }
                break
            case .release:
                if isSearching {
                    moviesSearch = moviesSearch.sorted(by: { $0.details?.releaseDate ?? Date(timeIntervalSince1970: 0) < $1.details?.releaseDate ?? Date(timeIntervalSince1970: 0) })
                } else {
                    movies = movies.sorted(by: { $0.details?.releaseDate ?? Date(timeIntervalSince1970: 0) > $1.details?.releaseDate ?? Date(timeIntervalSince1970: 0) })
                }
                break
            case .rate:
                if isSearching {
                    moviesSearch = moviesSearch.sorted(by: { $0.details?.voteAverage ?? 0 > $1.details?.voteAverage ?? 0 })
                } else {
                    movies = movies.sorted(by: { $0.details?.voteAverage ?? 0 > $1.details?.voteAverage ?? 0 })
                }
                break
            }
            collectionView.reloadData()
        }
    }
    
    var refreshControl = UIRefreshControl()
    
    //Pagination settings
    var page = 1
    let pageLimit = BaseRequest.defaultPageLimit
    var isGettingMore = false
    var canGetMore = false
    
    //Search
    var isSearching = false
    var searchController : UISearchController!
    var lastRequest: DataRequest?
    
    
    // MARK: - View life cycle
    
    override func viewWillAppear(_ animated: Bool) {
        if isSearching, #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = false
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        instantiateRefreshControl()
        instantiateSearchController()
        title = "\(searchList.rawValue) Movies"
        
        getMovies(page)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if searchController.searchBar.isFirstResponder { searchController.searchBar.resignFirstResponder() }
        if isSearching, #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
        }
    }
    
    
    // MARK: - Requests
    
    func getMovies(_ page: Int) {
        MoviesRequest.getMovies(page: page, searchList: searchList) { (error, movies) in
            if let error = error {
                print("comp error get movies \(error)")
            }
            
            if let movies = movies as? [Movie] {
//                print("comp movies \(movies)")
                self.movies = page == 1 ? movies : self.movies + movies
                self.canGetMore = !movies.isEmpty
                self.collectionView.reloadData()
            }
            
            self.isGettingMore = false
            self.refreshControl.endRefreshing()
        }
    }
    
    func getMoviesBy(_ search: String) {
        lastRequest?.cancel()
        MoviesRequest.getBySearch(search, completionRequest: { (request) in
            //Store this last request to cancel it.
            self.lastRequest = request
        }) { (error, movies) in
            if let error = error {
                print("comp error get movies \(error)")
            }
            
            if let movies = movies as? [Movie] {
                //                print("comp movies \(movies)")
                self.moviesSearch = movies
                self.collectionView.reloadData()
            }
            self.refreshControl.endRefreshing()
        }
    }
    
    func getDetails(for movie: Movie) {
        MoviesRequest.getDetails(movieID: movie.id) { (error, details) in
            if let error = error {
                print("comp error get details \(movie) \(error)")
                movie.canGetDetails = true
            }
            
            if let details = details as? Details {
//                print("comp details \(movie.title): \(details)")
                movie.details = details
                self.delay(time: 0.1, {
                    self.collectionView.reloadData()
                })
                
            }
        }
    }
    
    
    // MARK: - Refresh Control
    
    func instantiateRefreshControl() {
        refreshControl.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        collectionView.refreshControl = refreshControl
        //Or for iOS < 10
//        collectionView.addSubview(refreshControl)
//        self.collectionView.sendSubview(toBack: self.refreshControl)
        
    }
    
    @objc func refreshData() {
        if isSearching {
            getMoviesBy(searchController.searchBar.text.unwrapped)
        } else {
            page = 1
            getMovies(page)
        }
    }
    
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? DetailsViewController,
            let cell = sender as? UICollectionViewCell,
            let indexPath = collectionView.indexPath(for: cell),
            let movie = isSearching ? moviesSearch.elementAtIndex(i: indexPath.row) : movies.elementAtIndex(i: indexPath.row) {
            destination.movie = movie
        }
    }
    
    
    // MARK: - Actions
    
    @IBAction func actList(_ sender: Any) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        SearchList.allValues.forEach { (searchList) in
            alert.addAction(UIAlertAction(title: searchList.rawValue, style: self.searchList == searchList ? .destructive : .default) { _ in
                guard self.searchList != searchList else { return }
                self.title = "\(searchList.rawValue) Movies"
                self.searchList = searchList
                self.page = 1
                self.getMovies(self.page)
            })
        }
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        present(alert)
    }
    
    @IBAction func actOrder(_ sender: Any) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        Order.allValues.forEach { (order) in
            alert.addAction(UIAlertAction(title: order.rawValue, style: self.order == order ? .destructive : .default) { _ in
                guard self.order != order else { return }
                self.order = order
            })
        }
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        present(alert)
    }
    
    @IBAction func actSearch(_ sender: Any) {
        navigationItem.rightBarButtonItem = nil
        self.navigationItem.titleView = searchController.searchBar
        searchController.searchBar.becomeFirstResponder()
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = false
        }
    }
}


// MARK: - Collection view data source

extension MoviesViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return isSearching ? moviesSearch.count : movies.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MovieCollectionViewCell.reuseIdentifier, for: indexPath) as! MovieCollectionViewCell
        let movie = isSearching ? moviesSearch[indexPath.row] : movies[indexPath.row]
        
        cell.movie = movie
        
        if movie.canGetDetails {
            movie.canGetDetails = false
            getDetails(for: movie)
        }
        
        return cell
    }
}


// MARK: - Collection view delegate

//extension MoviesViewController: UICollectionViewDelegate {
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        
//    }
//}


// MARK: - Collection view delegate flow layout

extension MoviesViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.getCellSizeByRatio(defaultSize: MovieCollectionViewCell.Layout.defaultSize, cellInsetLeft: MovieCollectionViewCell.Layout.insetsLeft, cellInsetRight: MovieCollectionViewCell.Layout.insetsRight, spaceBetweenCells: MovieCollectionViewCell.Layout.spacing)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionElementKindSectionFooter {
            return collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "collectionFooter", for: indexPath)
        } else {
            return UICollectionReusableView()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        return canGetMore /*&& movies.count > pageLimit*/ && isSearching == false ? CGSize(width: view.frame.width, height: 50) : .zero
    }
}


// MARK: - Scroll view delegate

extension MoviesViewController: UIScrollViewDelegate {
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        if searchController.searchBar.isFirstResponder { searchController.searchBar.resignFirstResponder() }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        //Get more
        let bottomEdge = scrollView.contentOffset.y + scrollView.frame.size.height
        if (bottomEdge >= scrollView.contentSize.height - 100) {
            // we are at the end
            if canGetMore, isGettingMore == false, isSearching == false, movies.count >= pageLimit  {
                page += 1
                print("requesting get more page \(page)")
                isGettingMore = true
                getMovies(page)
            }
        }
    }
}


// MARK: - Search controller delegate

extension MoviesViewController: UISearchControllerDelegate, UISearchResultsUpdating, UISearchBarDelegate {
    func instantiateSearchController() {
        self.searchController = UISearchController(searchResultsController:  nil)
        self.searchController.searchResultsUpdater = self
        self.searchController.delegate = self
        self.searchController.searchBar.delegate = self
        self.searchController.searchBar.barStyle = .black
        self.searchController.searchBar.keyboardAppearance = .dark
        self.searchController.hidesNavigationBarDuringPresentation = false
        self.searchController.dimsBackgroundDuringPresentation = false
        
        self.definesPresentationContext = true
        searchController.searchBar.placeholder = "Search a film"
        navigationItem.rightBarButtonItem = searchBarButtonItem
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        if let text = searchController.searchBar.text, (text.count >= 3) {
//            print("search \(text)")
            getMoviesBy(text)
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        //Dismiss animation
        let fadeAnimation = CATransition()
        fadeAnimation.duration = 0.3
        fadeAnimation.type = kCATransitionFade
        navigationController?.navigationBar.layer.add(fadeAnimation, forKey: "fadeText")
        navigationItem.titleView = nil
        navigationItem.rightBarButtonItem = searchBarButtonItem
        
        isSearching = false
        navigationItem.leftBarButtonItems = [listBarButtonItem, orderBarButtonItem]
        collectionView.reloadDataAnimated()
        
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
        }
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        isSearching = true
        navigationItem.leftBarButtonItems = [orderBarButtonItem]
        collectionView.reloadDataAnimated()
    }
}




