//
//  Genre.swift
//  WGMovies
//
//  Created by Wesley Gomes on 29/08/17.
//  Copyright © 2017 Wesley Gomes. All rights reserved.
//

import Foundation
import Tailor

struct Genre: Mappable {
    var id = Int()
    var name = String()
    
    init(_ map: [String : Any]) {
        id <- map.property("id")
        name <- map.property("name")
    }
}
