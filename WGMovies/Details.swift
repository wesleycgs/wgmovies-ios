//
//  Details.swift
//  WGMovies
//
//  Created by Wesley Gomes on 28/08/17.
//  Copyright © 2017 Wesley Gomes. All rights reserved.
//

import Foundation
import Tailor

struct Details: Mappable {
    var overview: String?
    var posterPath: String?
    var releaseDate: Date?
    var runtime: Int?
    var tagline: String?
    var voteAverage: Double?
    var genres: [Genre] = []
    
    var genresFormatted: String? {
        guard genres.isEmpty == false else { return nil }
        var genresString = ""
        
        genres.forEach { (genre) in
            if genres.first?.id != genre.id { //Not first, append ", "
                genresString += ", "
            }
            genresString += genre.name
        }
        
        genresString += "."
        return genresString
    }
    
    var posterURL: URL? {
        guard let path = posterPath else { return nil }
        return URL(string: "\(BaseRequest.baseTmdbImage)\(path)")
    }
    
    init(_ map: [String : Any]) {
        overview <- map.property("overview")
        posterPath <- map.property("poster_path")
        releaseDate <- DateUtils.date(from: map.property("release_date") ?? "", formatterOfDateString: .usDate)
        runtime <- map.property("runtime")
        tagline <- map.property("tagline")
        voteAverage <- map.property("vote_average")
        genres <- map.relations("genres")
    }
}








