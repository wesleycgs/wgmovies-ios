//
//  MovieCollectionViewCell.swift
//  WGMovies
//
//  Created by Wesley Gomes on 28/08/17.
//  Copyright © 2017 Wesley Gomes. All rights reserved.
//

import UIKit
import SDWebImage

class MovieCollectionViewCell: UICollectionViewCell {
    
    static let reuseIdentifier = String(describing: MovieCollectionViewCell.self)
    
    //Setted in Storyboad
    enum Layout {
        static let defaultSize = CGSize(width: 150, height: 225)
        static let insetsLeft: CGFloat = 8
        static let insetsRight: CGFloat = 8
        static let spacing: CGFloat = 8
    }
    
    
    // MARK: - Outlets
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    
    var movie: Movie? {
        didSet {
            imageView.sd_setImage(with: movie?.details?.posterURL)
            titleLabel.text = movie?.title
            if let date = movie?.details?.releaseDate {
                detailLabel.text = DateUtils.string(from: date, formatter: .year)
            } else {
                detailLabel.text = nil
            }
        }
    }
}





