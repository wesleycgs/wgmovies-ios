//
//  MoviesRequest.swift
//  WGMovies
//
//  Created by Wesley Gomes on 28/08/17.
//  Copyright © 2017 Wesley Gomes. All rights reserved.
//

import Foundation
import Alamofire

class MoviesRequest: BaseRequest {
    
    // MARK: - URLs
    
    enum URLs {
        static let getPopular = "\(base)/movies/popular"
        static let getTrending = "\(base)/movies/trending"
        static let getMostPlayed = "\(base)/movies/played/weekly"
        
        static let getBySearch = "\(base)/search"
        
        static func getDatails(id: Int) -> String {
            return "\(baseTmdb)/movie/\(id)"
        }
    }
    
    
    // MARK: - Requests
    
    static func getMovies(page: Int, searchList: SearchList, _ completion: @escaping defaultArrayCallback) {
        switch searchList {
        case .popular:
            getPopular(page: page, completion)
            break
        case .trending:
            getTrending(page: page, completion)
            break
        case .mostPlayed:
            getMostPlayed(page: page, completion)
            break
        }
        
    }
    
    fileprivate static func getPopular(page: Int, _ completion: @escaping defaultArrayCallback) {
        let url = URLs.getPopular
        print("Requesting... \(url)")
        
        let params = [
            "page" : page,
            "limit" : defaultPageLimit
        ]
        
        print("params: \(params)")
        
        func genericError() {
            completion(NSError(domain: "An error ocurred\nTry again later", code: 999, userInfo: nil), nil)
            return
        }
        
        Alamofire.request(url, method: .get, parameters: params, encoding: URLEncoding.default, headers: self.instance.headersSet).responseJSON { (responseData) in
            
//            print("response: \(responseData)")
            
            switch responseData.result {
            case .success(let JSON):
                //Error
                guard let result = JSON as? JSONArray else {
                    genericError()
                    debugFailedResponse(responseData)
                    return
                }
                
                //Success
                completion(nil, result.compactMap(Movie.init))
                break
                
            default:
                genericError()
                debugFailedResponse(responseData)
                break
            }
        }
    }
    
    fileprivate static func getTrending(page: Int, _ completion: @escaping defaultArrayCallback) {
        let url = URLs.getTrending
        print("Requesting... \(url)")
        
        let params = [
            "page" : page,
            "limit" : defaultPageLimit
        ]
        
        print("params: \(params)")
        
        func genericError() {
            completion(NSError(domain: "An error ocurred\nTry again later", code: 999, userInfo: nil), nil)
            return
        }
        
        Alamofire.request(url, method: .get, parameters: params, encoding: URLEncoding.default, headers: self.instance.headersSet).responseJSON { (responseData) in
            
            //            print("response: \(responseData)")
            
            switch responseData.result {
            case .success(let JSON):
                //Error
                guard let result = JSON as? JSONArray else {
                    genericError()
                    debugFailedResponse(responseData)
                    return
                }
                
                //Success
                var movies: [Movie] = []
                result.forEach({ trendingObj in
                    if let movieObj = trendingObj.dictionary("movie") {
                        movies.append(Movie(movieObj))
                    }
                })
                
                completion(nil, movies)
                break
                
            default:
                genericError()
                debugFailedResponse(responseData)
                break
            }
        }
    }
    
    fileprivate static func getMostPlayed(page: Int, _ completion: @escaping defaultArrayCallback) {
        let url = URLs.getMostPlayed
        print("Requesting... \(url)")
        
        let params = [
            "page" : page,
            "limit" : defaultPageLimit
        ]
        
        print("params: \(params)")
        
        func genericError() {
            completion(NSError(domain: "An error ocurred\nTry again later", code: 999, userInfo: nil), nil)
            return
        }
        
        Alamofire.request(url, method: .get, parameters: params, encoding: URLEncoding.default, headers: self.instance.headersSet).responseJSON { (responseData) in
            
            //            print("response: \(responseData)")
            
            switch responseData.result {
            case .success(let JSON):
                //Error
                guard let result = JSON as? JSONArray else {
                    genericError()
                    debugFailedResponse(responseData)
                    return
                }
                
                //Success
                var movies: [Movie] = []
                result.forEach({ trendingObj in
                    if let movieObj = trendingObj.dictionary("movie") {
                        movies.append(Movie(movieObj))
                    }
                })
                
                completion(nil, movies)
                break
                
            default:
                genericError()
                debugFailedResponse(responseData)
                break
            }
        }
    }
    
    static func getBySearch(_ search: String, completionRequest: ((_ request: DataRequest?) -> Void)? = nil , _ completion: @escaping defaultArrayCallback) {
        let url = URLs.getBySearch
        print("Requesting... \(url)")
        
        let params = [
            "type" : "movie",
            "query" : search
        ]
        
        print("params: \(params)")
        
        func genericError() {
            completion(NSError(domain: "An error ocurred\nTry again later", code: 999, userInfo: nil), nil)
            return
        }
        
        let request = Alamofire.request(url, method: .get, parameters: params, encoding: URLEncoding.default, headers: self.instance.headersSet).responseJSON { (responseData) in
            
            //            print("response: \(responseData)")
            
            switch responseData.result {
            case .success(let JSON):
                //Error
                guard let result = JSON as? JSONArray else {
                    genericError()
                    debugFailedResponse(responseData)
                    return
                }
                
                //Success
                var movies: [Movie] = []
                result.forEach({ searchObj in
                    if let movieObj = searchObj.dictionary("movie") {
                        movies.append(Movie(movieObj))
                    }
                })
                
                completion(nil, movies)
                break
                
            default:
                genericError()
                debugFailedResponse(responseData)
                break
            }
        }
        
        completionRequest?(request)
    }
    
    static func getDetails(movieID: Int, _ completion: @escaping defaultItemCallback) {
        let url = URLs.getDatails(id: movieID)
        print("Requesting... \(url)")
        
        let params = [
            "api_key" : tmdbApiKey
        ]
        
        print("params: \(params)")
        
        func genericError() {
            completion(NSError(domain: "An error ocurred\nTry again later", code: 999, userInfo: nil), nil)
            return
        }
        
        let headersSet: HTTPHeaders = [
            "Content-Type": "application/json",
        ]
        
        Alamofire.request(url, method: .get, parameters: params, encoding: URLEncoding.default, headers: headersSet).responseJSON { (responseData) in
            
            print("response: \(responseData)")
            
            switch responseData.result {
            case .success(let JSON):
                //Error
                guard let result = JSON as? JSONDictionary else {
                    genericError()
                    debugFailedResponse(responseData)
                    return
                }
                
                //Success
                completion(nil, Details(result))
                break
                
            default:
                genericError()
                debugFailedResponse(responseData)
                break
            }
        }
    }
    
    
}
