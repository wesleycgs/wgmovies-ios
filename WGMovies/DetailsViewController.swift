//
//  DetailsViewController.swift
//  WGMovies
//
//  Created by Wesley Gomes on 29/08/17.
//  Copyright © 2017 Wesley Gomes. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController {

    var movie: Movie! //Pass by segue
    
    // MARK: - Outlets
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var taglineLabel: UILabel!
    @IBOutlet weak var releaseDateLabel: UILabel!
    @IBOutlet weak var runtimeLabel: UILabel!
    @IBOutlet weak var genreLabel: UILabel!
    @IBOutlet weak var overviewLabel: UILabel!
    @IBOutlet weak var rateLabel: UILabel!
    
    var refreshControl = UIRefreshControl()
    
    
    // MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard movie != nil else {
            assert(false, "pass `movie: Movie` by segue!")
            return
        }
        instantiateRefreshControl()
        
        guard movie.details?.overview != nil else {
            getDetails()
            return
        }
        reloadViews()
    }
    
    func reloadViews() {
        let details = movie.details
        title = movie.title
        
        UIImageView().sd_setImage(with: details?.posterURL) { (image, _, _, _) in
            if let image = image {
                self.delay(time: 0.2, {
                    self.imageView.setImageAnimated(image)
                })
            }
        }
        
        taglineLabel.text = details?.tagline
        runtimeLabel.text = details?.runtime?.timeFormatMinutesFormatted
        overviewLabel.text = details?.overview
        genreLabel.text = details?.genresFormatted
        
        rateLabel.isHidden = details?.voteAverage == nil
        if let average = details?.voteAverage {
            rateLabel.text = String(format: "%.1f", average).replacingOccurrences(of: ".", with: ",")
        }
        
        releaseDateLabel.isHidden = details?.releaseDate == nil
        if let date = details?.releaseDate {
            releaseDateLabel.text = DateUtils.string(from: date, formatter: .d_MMM_yyyy)
        }
    }
    
    
    // MARK: - Requests
    
    func getDetails() {
        MoviesRequest.getDetails(movieID: movie.id) { (error, details) in
            if let error = error {
                self.refreshControl.endRefreshing()
                print("comp error get details \(self.movie) \(error)")
            }
            
            if let details = details as? Details {
                print("comp details \(self.movie.title): \(details)")
                self.movie.details = details
                
                self.delay(time: 1, {
                    self.reloadViews()
                    self.contentView.setAlphaAnimated(1)
                    self.refreshControl.endRefreshing()
                })
            }
        }
    }
    
    
    // MARK: - Refresh Control
    
    func instantiateRefreshControl() {
        refreshControl.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        scrollView.addSubview(refreshControl)
        scrollView.sendSubview(toBack: refreshControl)
    }
    
    @objc func refreshData() {
        contentView.setAlphaAnimated(0)
        getDetails()
    }
}













