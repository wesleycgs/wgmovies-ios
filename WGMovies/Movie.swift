//
//  Movie.swift
//  WGMovies
//
//  Created by Wesley Gomes on 28/08/17.
//  Copyright © 2017 Wesley Gomes. All rights reserved.
//

import Foundation
import Tailor

class Movie: Mappable {
    var id = Int()
    var title = String()
    var year = Int()
    var details: Details? //Manually    
    var canGetDetails = true
    
    required init(_ map: [String : Any]) {
        title <- map.property("title")
        year <- map.property("year")
        id <- map.resolve(keyPath: "ids.tmdb")
    }
}
