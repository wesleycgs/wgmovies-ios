//
//  BaseRequest.swift
//
//
//  Created by Wesley Gomes on 17/07/17.
//  Copyright © 2017 Megaleios. All rights reserved.
//

import Foundation
import Alamofire

typealias JSONDictionary = [String : Any]
typealias JSONArray = [[String : Any]]

//To send Array of JSON as Parameters
struct JSONDocumentArrayEncoding: ParameterEncoding {
    private let array: [Any]
    init(array:[Any]) {
        self.array = array
    }
    func encode(_ urlRequest: URLRequestConvertible, with parameters: Parameters?) throws -> URLRequest {
        var urlRequest = urlRequest.urlRequest
        
        let data = try JSONSerialization.data(withJSONObject: array, options: [])
        
        if urlRequest!.value(forHTTPHeaderField: "Content-Type") == nil {
            urlRequest!.setValue("application/json", forHTTPHeaderField: "Content-Type")
        }
        
        urlRequest!.httpBody = data
        
        return urlRequest!
    }
}

class BaseRequest {
    
    typealias defaultArrayCallback = (_ error: NSError?, _ result: [Any]?) -> Void
    typealias defaultItemCallback = (_ error: NSError?, _ result: Any?) -> Void
    
    // MARK: - Config
    
    class var instance: BaseRequest {
        struct Static {
            static let instance = BaseRequest()
        }
        return Static.instance
    }
    
    static let base = "https://api.trakt.tv"
    static let traktApiKey = "78d31ae2a0cf7eb6c23b9c14ba1a1fcc676b328e73bc0574ad4fa7c1a3c1427a"
    
    static let baseTmdb = "https://api.themoviedb.org/3"
    static let baseTmdbImage = "https://image.tmdb.org/t/p/w300"    
    static let tmdbApiKey = "76d72bc03c2b0d73be702f9430a4783d"
    
    static let defaultPageLimit = 20
    
    var headersSet: HTTPHeaders = [
//        "Authorization": KeychainsManager.getAccesToken() ?? "",
        "Content-Type": "application/json",
        "trakt-api-key": BaseRequest.traktApiKey
    ]
    
    static func clearAccessToken() {
        self.instance.headersSet["Authorization"] = ""
    }
    
    static func debugFailedResponse(_ response: DataResponse<Any>) {
        print("\n== ERROR REQUEST ==")
        print("RESPONSE \(String(describing: response))")
        print("URL \(String(describing: response.request?.url))")
        print("CODE \(String(describing: response.response?.statusCode))")
        print("== == ==\n")
    }
}








