//
//  Extensions.swift
//
//
//  Created by Wesley Gomes on 07/07/16.
//  Copyright © 2016 Wesley Gomes. All rights reserved.
//

import UIKit

// MARK: - String

extension String {
    ///Remove white spaces from String
    func trim() -> String {
        return self.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    
    func emptyOrNil() -> String? {
        return self.trim().isEmpty ? nil : self
    }
    
    // MARK: Phone
    mutating func removePhoneMask() -> String {
        let chars: [String] = [" ", "-", "(", ")", "+"]
        return self.replacingOccurrences(ofStrings: chars, with: "")
    }
    
//    mutating func getPhoneDDD() -> String? {
//        if Int(self) == nil, self.characters.count < 2 {
//            return nil
//        }
//
//        let index = self.index(self.startIndex, offsetBy: 2)
//        let sub = self.substring(to: index)
//        return sub
//    }
    
    mutating func replacingOccurrences(ofStrings strings: [String], with replaceString: String) -> String {
        for string in strings {
            self = self.replacingOccurrences(of: string, with: replaceString)
        }
        return self
    }
    
    func convertDateFormater(date: String, originalFormat: String, desiredFormat: String) -> String {
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = originalFormat
        dateFormatter.locale = Locale.init(identifier: "pt_BR")
        let originalDate = dateFormatter.date(from: date)
        
        dateFormatter.dateFormat = desiredFormat
        let dateString = dateFormatter.string(from: originalDate!)
        
        return dateString
    }
    
    // MARK: Getter only
    
    var isValidEmail: Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: self)
    }
    
//    var isValidCPF: Bool {
//        let numbers = characters.flatMap({Int(String($0))})
//        guard numbers.count == 11 && Set(numbers).count != 1 else { return false }
//        let soma1 = 11 - ( numbers[0] * 10 +
//            numbers[1] * 9 +
//            numbers[2] * 8 +
//            numbers[3] * 7 +
//            numbers[4] * 6 +
//            numbers[5] * 5 +
//            numbers[6] * 4 +
//            numbers[7] * 3 +
//            numbers[8] * 2 ) % 11
//        let dv1 = soma1 > 9 ? 0 : soma1
//        let soma2 = 11 - ( numbers[0] * 11 +
//            numbers[1] * 10 +
//            numbers[2] * 9 +
//            numbers[3] * 8 +
//            numbers[4] * 7 +
//            numbers[5] * 6 +
//            numbers[6] * 5 +
//            numbers[7] * 4 +
//            numbers[8] * 3 +
//            numbers[9] * 2 ) % 11
//        let dv2 = soma2 > 9 ? 0 : soma2
//        return dv1 == numbers[9] && dv2 == numbers[10]
//    }
}


// MARK: - Array

///Get `optional` Element in Array, case index is out of range return nil safely, instead crashing your app.
extension Array {
    public func elementAtIndex(i: Int) -> Element? {
        return i < self.count && i >= 0 ? self[i] : nil
    }
}

extension Array where Element: Equatable {
    // Remove first collection element that is equal to the given `object`:
    mutating func remove(object: Element) {
        if let index = index(of: object) {
            remove(at: index)
        }
    }
}


// MARK: - ######## UIKit ########

// MARK: - UIViewController

extension UIViewController {
    
    func whiteNavBar(tabBar: Bool = false) {
        UINavigationBar().frame = CGRect(x: 0, y: -20, width: self.view.frame.width, height: 70)
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.tintColor = UIColor(red: 153/255, green: 153/255, blue: 153/255, alpha: 1)
        
        UIApplication.shared.statusBarStyle = .default
//        UIApplication.shared.setStatusBarStyle(UIStatusBarStyle.default, animated: true)
        
        self.view.setNavBarShadow(height: 20)
        
        if tabBar {
            self.view.setTabBarShadow()
        }
    }
    
    func transparentNavBar() {
        UIApplication.shared.statusBarStyle = .lightContent
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.backgroundColor = .clear
        
        UIApplication.shared.statusBarStyle = .default
//        UIApplication.shared.setStatusBarStyle(UIStatusBarStyle.lightContent, animated: true)
        
        
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    func delay(time: TimeInterval, _ execute: @escaping (() -> Void)) {
        DispatchQueue.main.asyncAfter(deadline: .now()+time, execute: execute)
    }
    
    func present(_ viewController: UIViewController) {
        self.present(viewController, animated: true)
    }
    
    func dismiss() {
        self.dismiss(animated: true)
    }
    
    //MARK: Getter only
    
    // Check if ViewController is current visible
    var isVisible: Bool {
        return self.isViewLoaded && self.view.window != nil
    }
    
    var statusBarHeight: CGFloat {
        return UIApplication.shared.statusBarFrame.height
    }
    
    var navigationControllerBarHeight: CGFloat {
        return (navigationController?.navigationBar.frame.origin.y ?? 0.0) + (navigationController?.navigationBar.frame.height ?? 0.0)
    }
    
    var tabBarHeight: CGFloat {
        return tabBarController?.tabBar.frame.height ?? 0
    }
    
}


// MARK: - UIView

extension UIView {
    
    ///Set alpha animated, default durations is 0.3
    func setAlphaAnimated(_ alpha: CGFloat) {
        
        //Do nothing if is already set value
        if self.alpha == alpha {
            return
        }
        
        UIView.animate(withDuration: 0.3, delay: 0,
                       options: .curveEaseOut,
                       animations: {
                        self.alpha = alpha
        }, completion: nil)
    }
    
    ///Set alpha animated with duration
    func setAlphaAnimated(_ alpha: CGFloat, duration: TimeInterval) {
        
        //Do nothing if is already set value
        if self.alpha == alpha {
            return
        }
        
        UIView.animate(withDuration: duration, delay: 0,
                       options: .curveEaseOut,
                       animations: {
                        self.alpha = alpha
        }, completion: nil)
    }
    
    func setMotionEffect(value: Double) {
        // Set vertical effect
        let verticalMotionEffect = UIInterpolatingMotionEffect(keyPath: "center.y",
                                                               type: .tiltAlongVerticalAxis)
        verticalMotionEffect.minimumRelativeValue = -value
        verticalMotionEffect.maximumRelativeValue = value
        
        // Set horizontal effect
        let horizontalMotionEffect = UIInterpolatingMotionEffect(keyPath: "center.x",
                                                                 type: .tiltAlongHorizontalAxis)
        horizontalMotionEffect.minimumRelativeValue = -value
        horizontalMotionEffect.maximumRelativeValue = value
        
        // Create group to combine both
        let group = UIMotionEffectGroup()
        group.motionEffects = [horizontalMotionEffect, verticalMotionEffect]
        
        // Add both effects to your view
        self.addMotionEffect(group)
    }
    
    func applyGradient(colours: [UIColor], radius: Float) -> Void {
        self.applyGradient(colours: colours, locations: nil, radius: radius)
    }
    
    func applyGradient(colours: [UIColor], locations: [NSNumber]?, radius: Float) -> Void {
        let gradient: CAGradientLayer = CAGradientLayer()
        
        gradient.frame = self.bounds
        gradient.cornerRadius = CGFloat(radius)
        gradient.colors = colours.map { $0.cgColor }
        gradient.startPoint = CGPoint(x: 0.0,y :0.5)
        gradient.endPoint = CGPoint(x: 1.0,y :0.5)
        
        self.layer.insertSublayer(gradient, at: 0)
    }
    
    func setNavBarShadow(height: CGFloat) {
        let customView = UIView(frame: CGRect(x: 0, y: -18, width: self.frame.size.width, height: height))
        
        customView.layer.shadowColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.05).cgColor
        customView.layer.shadowOpacity = 1
        customView.layer.shadowOffset = CGSize.zero
        customView.layer.shadowRadius = 2
        customView.layer.shadowPath = UIBezierPath(rect: customView.bounds).cgPath
        customView.layer.shouldRasterize = true
        
        self.addSubview(customView)
    }
    
    func setTabBarShadow() {
        let customView = UIView(frame: CGRect(x: 0, y: self.frame.size.height - 115, width: self.frame.size.width, height: 120))
        
        customView.layer.shadowColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.05).cgColor
        customView.layer.shadowOpacity = 1
        customView.layer.shadowOffset = CGSize.zero
        customView.layer.shadowRadius = 4
        customView.layer.shadowPath = UIBezierPath(rect: customView.bounds).cgPath
        customView.layer.shouldRasterize = true
        
        self.addSubview(customView)
    }
    
    func applyShadow() {
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize.zero
        self.layer.shadowOpacity = 0.05
        self.layer.shadowRadius = 4
    }
    
    func animateTransition(time: TimeInterval? = nil) {
        let transition = CATransition()
        transition.duration = time ?? 1.0
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionFade
        self.layer.add(transition, forKey: nil)
    }
}


// MARK: - UIImageView

extension UIImageView {
    func tintImageColor(_ color: UIColor) {
        self.image = self.image!.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        self.tintColor = color
    }
    
    func setImageAnimated(_ image: UIImage) {
        self.image = image
        let transition = CATransition()
        transition.duration = 1.0
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionFade
        self.layer.add(transition, forKey: nil)
    }
}


extension UILabel {
//    func setHTMLText(_ textHTML: String) {
//
//        let fontName = /*self.font!.fontName*/ "HelveticaNeue"
//        let fontSize = /*self.font!.pointSize*/ CGFloat(15)
//
//        let modifiedFont = String(format:"<span style=\"font-family: \(fontName); font-size: \(fontSize)\">%@</span>", textHTML)
//        let attrStr = try! NSAttributedString(
//            data: modifiedFont.data(using: String.Encoding.unicode, allowLossyConversion: true)!,
//            options: [NSAttributedString.DocumentAttributeKey.documentType: NSAttributedString.DocumentType.html, NSAttributedString.DocumentAttributeKey.characterEncoding: String.Encoding.utf8.rawValue],
//            documentAttributes: nil)
//        self.attributedText = attrStr
//    }
}

// MARK: - UITextField

extension UITextField {
    func addCharactersSpacing(spacing: CGFloat, text: String) {
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttribute(NSAttributedStringKey.kern, value: spacing, range: NSMakeRange(0, text.count))
        self.attributedText = attributedString
    }
}

// MARK: - UITextView

extension UITextView {
//    func setHTMLText(_ textHTML: String) {
//
//        let fontName = /*self.font!.fontName*/ "HelveticaNeue"
//        let fontSize = /*self.font!.pointSize*/ CGFloat(15)
//
//        let modifiedFont = String(format:"<span style=\"font-family: \(fontName); font-size: \(fontSize)\">%@</span>", textHTML)
//        let attrStr = try! NSAttributedString(
//            data: modifiedFont.data(using: String.Encoding.unicode, allowLossyConversion: true)!,
//            options: [NSAttributedString.DocumentAttributeKey.documentType: NSAttributedString.DocumentType.html, NSAttributedString.DocumentAttributeKey.characterEncoding: String.Encoding.utf8.rawValue],
//            documentAttributes: nil)
//        self.attributedText = attrStr
//    }
}


// MARK: - UIScrollView

extension UIScrollView {
    func scrollToTop(animated: Bool) {
        setContentOffset(CGPoint(x: contentOffset.x, y: -contentInset.top), animated: animated)
    }
    
    func scrollToBottom(animated: Bool) {
        setContentOffset(CGPoint(x: contentOffset.x, y: contentSize.height - bounds.size.height + contentInset.bottom), animated: animated)
    }
}


// MARK: - UITableView

extension UITableView {
    /// Reloads rows of the section 0 with automatic animation
    func reloadDataAnimated(animation: UITableViewRowAnimation? = nil, animated: Bool? = nil) {
        guard let animated = animated, animated else {
            self.reloadData()
            return
        }
        self.reloadSections(IndexSet(integer: 0), with: animation ?? .automatic)
    }

    /// Return IndexPath for tableview in item clicked
    func indexPathClicked(_ sender: Any) -> IndexPath {
        let touchPoint = (sender as AnyObject).convert(CGPoint.zero, to: self)
        return self.indexPathForRow(at: touchPoint)!
    }
}


// MARK: - UICollectionView

extension UICollectionView {
    /// Reloads items of the section 0 with automatic animation
    func reloadDataAnimated() {
        self.reloadSections(IndexSet(integer: 0))
    }
    
    /// Return IndexPath for collectionview in item clicked
    func indexPathClicked(_ sender: Any) -> IndexPath {
        let touchPoint = (sender as AnyObject).convert(CGPoint.zero, to: self)
        return self.indexPathForItem(at: touchPoint)!
    }
    
    func getCellSizeByRatio(defaultSize: CGSize, cellInsetLeft: CGFloat, cellInsetRight: CGFloat, spaceBetweenCells: CGFloat, collectionViewSize: CGSize? = nil) -> CGSize {
        print("\n")
        
        let collWidth = self.frame.width
        var width: CGFloat = 0
        //let height: CGFloat = 192
        
        var cellsByLine = 4
        
        //Define cells by line by device screen size
        if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
            if UIApplication.shared.statusBarOrientation.isPortrait {
                cellsByLine = 2
            } else {
                cellsByLine = 4
            }
        } else { //IPAD
            if UIApplication.shared.statusBarOrientation.isPortrait {
                cellsByLine = 4
            } else {
                cellsByLine = 6
            }
        }
        
        
        //Calcule cell width
        let totalSpaceBetweenCells = spaceBetweenCells*(CGFloat(cellsByLine-1))
        width = (collWidth-totalSpaceBetweenCells-cellInsetLeft-cellInsetRight)/CGFloat(cellsByLine)
        
        
        //        print("cellsByLine: \(cellsByLine)")
        //        print("defaultSize: \(defaultSize)")
        //        print("cellInsetLeft: \(cellInsetLeft)")
        //        print("cellInsetRight: \(cellInsetRight)")
        //        print("spaceBetweenCells: \(spaceBetweenCells)")
        //        print("total space: \(totalSpaceBetweenCells)")
        //
        //
        //        print("collWidth: \(collWidth)")
        //
        //        print("totalSpaceBetweenCells: \(totalSpaceBetweenCells)")
        //        print("width will be: \(width)")
        
        
        
        var size = CGSize.zero
        
        //SQUARE, DONT CALC RATIO
        if defaultSize.width == defaultSize.height {
            size = CGSize(width: width, height: width)
        } else { //NOT SQUARE, CALC RATIO!
            //Calcule height by aspect ratio
            let oldWidth = defaultSize.width
            let oldHeight = defaultSize.height
            
            //let scaleFactor: CGFloat = (oldWidth > oldHeight) ? width / oldWidth : height / oldHeight;
            let scaleFactor: CGFloat = width / oldWidth
            
            let newHeight: CGFloat = oldHeight * scaleFactor;
            //let newWidth: CGFloat = oldWidth * scaleFactor;
            
            let newSize = CGSize(width: width, height: newHeight)
            //            print("newSize \(newSize)")
            
            size = newSize
        }
        
        //print("cell size \(size)")
        return size
    }
}

extension UITabBarController {
    
    func setTabBarVisible(_ visible: Bool, animated: Bool) {
        
        // bail if the current state matches the desired state
        if (tabBarIsVisible == visible) { return }
        
        // get a frame calculation ready
        let frame = self.tabBar.frame
        let height = frame.size.height
        let offsetY = (visible ? -height : height)
        
        // animate the tabBar
        UIView.animate(withDuration: animated ? 0.3 : 0.0) {
            self.tabBar.frame = frame.offsetBy(dx: 0, dy: offsetY)
            self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height + offsetY)
            self.view.setNeedsDisplay()
            self.view.layoutIfNeeded()
        }
    }
    
    
    // MARK: Getter only
    
    var tabBarIsVisible: Bool {
        return self.tabBar.frame.origin.y < self.view.frame.maxY
    }
}

extension UINavigationController {
    func setNavigationBarTranslucent(_ translucent: Bool) {
        if translucent {
            navigationBar.setBackgroundImage(UIImage(), for: .default)
            navigationBar.shadowImage = UIImage()
        } else {
            let bar = UIImage(named: "bar.png")?.stretchableImage(withLeftCapWidth: 0, topCapHeight: 0)
            navigationBar.setBackgroundImage(bar, for: .default)
        }
    }
}

// MARK: - Double
extension Double {
    func roundTo(places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
    
    
    func format(f: String) -> String {
        return String(format: "%\(f)f", self)
    }
    
    var currency: String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.locale = Locale.current
        return formatter.string(for: self) ?? ""
    }
    
    var currencyBR: String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.locale = Locale(identifier: "pt_BR")
        return formatter.string(for: self) ?? ""
    }
    
    var distanceFormat: String {
        return String(format: "%.2fkm", self/1000)
    }
}


// MARK: - Int

extension Int {
    
    //Seconds
    var timeFormatSeconds: String {
        let double = Double(self)
        
        let ti = NSInteger(self)
        let ms = Int((double.truncatingRemainder(dividingBy: 1)) * 1000)
        
        let seconds = ti % 60
        let minutes = (ti / 60) % 60
        let hours = (ti / 3600)
        
        return String(format: "%0.2d:%0.2d:%0.2d.%0.3d", hours, minutes, seconds, ms)
    }
    
    var timeFormatSecondsFormatted: String {
        let ti = NSInteger(self)
        let minutes = (ti / 60) % 60
        let hours = (ti / 3600)
        
        var durationString = hours > 0 ? String(format: "%dh", hours) : ""
        durationString += String(format: "%dmin", minutes)
        
        return durationString
    }
    
    //Minutes
    var timeFormatMinutesFormatted: String {
        let ti = NSInteger(self) * 60 //Convert to seconds
        let minutes = (ti / 60) % 60
        let hours = (ti / 3600)
        
        var durationString = hours > 0 ? String(format: "%dh", hours) : ""
        durationString += String(format: "%dmin", minutes)
        
        return durationString
    }
}


// MARK: - Wnwrap
extension Optional where Wrapped == String {
    var unwrapped: String {
        return self ?? ""
    }
}

extension Optional where Wrapped == Int {
    var unwrapped: Int {
        return self ?? 0
    }
}

extension Optional where Wrapped == Double {
    var unwrapped: Double {
        return self ?? 0
    }
}




