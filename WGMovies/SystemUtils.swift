//
//  SystemUtils.swift
//
//  Created by Wesley Gomes on 17/05/17.
//  Copyright © 2017 Megaleios. All rights reserved.
//

import Foundation
import UIKit

enum Storyboard: String {
    case main = "Main"
    case login = "Login"
}

class SystemUtils {
    
    static func logout() {
        //Remove push id
//        if let pushID = UserDefaults.standard.object(forKey: "pushID") as? String {
//            ProfileRequest.removePushId(pushID)
//        }
        
//        ProgressUtils.dismiss()
//        KeychainsManager.clearAll()
//        BaseRequest.clearAccessToken()
//        RootViewController.stopUpdatingLocation()
//        FirebaseUtils.stopAllObservers()
//        
//        UserRepository.setUser(nil)
        SystemUtils.goTo(storyboard: .login, transitionStyle: .crossDissolve, from: getTopMostController())
    }
    
    //Return current view controller window
    static func getTopMostController() -> UIViewController {
        var topController = UIApplication.shared.keyWindow!.rootViewController!
        while (topController.presentedViewController != nil) {
            topController = topController.presentedViewController!
        }
        return topController
    }
    
    static func get(storyboard: Storyboard) -> UIStoryboard {
        return UIStoryboard(name: storyboard.rawValue, bundle: nil)
    }
    
    static func getInitialViewControllerFor(storyboard: Storyboard) -> UIViewController? {
        let tmpStoryboard = UIStoryboard(name: storyboard.rawValue, bundle: nil)
        guard let initialViewController = tmpStoryboard.instantiateInitialViewController() else {
            print("Não há uma `initial view controller` setada para o storyboard `\(storyboard.rawValue)`")
            return nil
        }
        
        return initialViewController
    }
    
    static func goTo(storyboard: Storyboard, transitionStyle: UIModalTransitionStyle? = nil, from vc: UIViewController) {
        if let viewController = getInitialViewControllerFor(storyboard: storyboard) {
            if let transitionStyle = transitionStyle {
                viewController.modalTransitionStyle = transitionStyle
            }
            vc.present(viewController, animated: true, completion: nil)
        }
    }
    
}
