//
//  Enums.swift
//  WGMovies
//
//  Created by Wesley Gomes on 29/08/17.
//  Copyright © 2017 Wesley Gomes. All rights reserved.
//

import Foundation

//Movie search list
enum SearchList: String {
    case popular = "Popular"
    case trending = "Trending"
    case mostPlayed = "Most Played"
    
    static let allValues = [popular, trending, mostPlayed]
}

enum Order: String {
    case title = "Title"
    case release = "Release Date"
    case rate = "Average Rating"
    
    static let allValues = [title, release, rate]
}
